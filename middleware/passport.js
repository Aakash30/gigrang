'use strict';
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./../models/users');
module.exports = function (passport) {  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = "secretkey";
  opts.passReqToCallback = true;  
  passport.use(new JwtStrategy(opts, async function (req, jwt_payload, done) {
    let token = req.headers.authorization.split(' ')[1];
    let auth_token;
    auth_token = await User.query().where('token', token).andWhere('user_id',jwt_payload.id).first();
    if (auth_token) {
      req.id = auth_token.user_id;
      req.user_type=auth_token.user_type
      return done(null, auth_token);
    } 
    else {
      return done(null, false);
    }
  }));
}