const gcm = require('node-gcm');

const settings = {
  gcm: {
    id: 'AAAASLTQ-Us:APA91bFLU3w8CRS4SeXgN3OcH-3fLDIDowfVR4jIZ-xsBhw0zUnH8WTDkJ0dJToKa_TNaemMiJMnV5lgMC0_Od_-dXtOvttiG6QzOk3knmZPuiOiZ8d7XCevPGTM5OpnP9D7yWBoO5CU' // AIzaSyA4JAhTu0d520QcPp3JlThrBHOAu8aFt1I
  }
};
const PushNotifications = require('node-pushnotifications');
const push = new PushNotifications(settings);
//const apn = require('apn');

var options = {
  // token: {
  //   key: "./certs/AuthKey_J2HR3T4NF8.p8",
  //   keyId: "J2HR3T4NF8",
  //   teamId: "6C6S2874ZZ"
  // },
  // production: false
};

// var apnProvider = new apn.Provider(options);
var gcmSender = new gcm.Sender(settings.gcm.id);
module.exports.push = push;


module.exports.sendPushNotification = async (notif, options) => {
  let err;
  const device_type = 'Android';
  const device = notif.deviceToken;
  if (device_type == 'Android') {

    var message = new gcm.Message({
      collapseKey: 'demo',
      priority: 'high',
      contentAvailable: true,
      delayWhileIdle: true,
      restrictedPackageName: 'com.ebabu.remotedeveloper',
      data: {
        body: notif.content
      }
    });

    var regTokens = [device];
    gcmSender.sendNoRetry(message, { registrationTokens: regTokens }, function (err, response) {
      if (err) console.error(err);
      else 
      console.log(response);
    });
  }
}


/**Web push notification */
module.exports.sendPushNotifyRequestWeb = async (notif, options) => {
  var http = require("https");
  var options = {
    "method": "POST",
    "hostname": "fcm.googleapis.com",
    // "port": null,
    "path": "/fcm/send",
    "headers": {
      "content-type": "application/json",
      "Authorization": "key=AAAASLTQ-Us:APA91bFLU3w8CRS4SeXgN3OcH-3fLDIDowfVR4jIZ-xsBhw0zUnH8WTDkJ0dJToKa_TNaemMiJMnV5lgMC0_Od_-dXtOvttiG6QzOk3knmZPuiOiZ8d7XCevPGTM5OpnP9D7yWBoO5CU",
      "cache-control": "no-cache"
    }
  };

  // for (let index = 0; index < notif.token.length; index++) {
  var req = await http.request(options, function (res) {
    var chunks = [];
    res.on("data", function (chunk) {
      chunks.push(chunk);
    });
    res.on("end", function () {
      //console.log(chunks);
      var body = Buffer.concat(chunks);
      // console.log(JSON.parse(body));
    });
  });
  let bodyData = {};
  const element = notif.token;
  bodyData = JSON.stringify({
    "data": {
      "notification": {
        "body": notif.body,
        "title": notif.title,
        // "icon": '/var/www/html/moki_api/default_image.png',
        //"click_action": notif.url
      }
    },
    "to": element
  });
  await req.write(bodyData);
  req.end();
}
  //req.write(bodyData);
  // req.end();
//}

