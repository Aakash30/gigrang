const sgMail = require('@sendgrid/mail');
const sendgrid = require('../config/config')
sgMail.setApiKey(sendgrid);
module.exports.sendEmail = async (to,code) => {
  const msg = {
    to: to,
    from: 'no-reply@gigrang.com',
    subject: "Welcome to Gigrang,Your Otp is here",
    text: "Please Find the Otp below",
    html: '<p>Your Otp is '+code+'</p>'
  };
  sgMail.send(msg)
    .then(() => {
      //Celebrate
    })
    .catch(error => {      //Log friendly error
      console.error(error.toString());      //Extract error msg
      const { message, code, response } = error;      //Extract response msg
      const { headers, body } = response;
    });
}