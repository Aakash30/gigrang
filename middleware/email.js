const sgMail = require('@sendgrid/mail');
const sendgrid = require('../config/config')
sgMail.setApiKey(sendgrid);
module.exports.sendEmail = async (to,code) => {
  const msg = {
    to: to,
    from: 'no-reply@gigrang.com',
    subject: "Welcome to Gigrang,please verify your account",
    text: "Please Verify the account by clicking on the below link",
    html: '<p>Verify Account By <a href="http://13.232.11.227/api/user/verify?verification_code='+code+'&email='+to+'">Clicking here</a></p>'
  };
  sgMail.send(msg)
    .then(() => {
      //Celebrate
    })
    .catch(error => {      //Log friendly error
      console.error(error.toString());      //Extract error msg
      const { message, code, response } = error;      //Extract response msg
      const { headers, body } = response;
    });
}