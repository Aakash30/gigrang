const UserProfile = require("../../models/users")
const getuserprofileinformation = function (req, res) {
    const id=req.id;
    try {
        getuserprofiledata(id, (err, data) => {
            if (err) {
                console.log(err)
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        console.log(e)
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserprofiledata = async (id, callback) => {
    try {
        const user = await UserProfile.query().findOne({ "user_id": id });
        if (user) {
            const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no,"img_url":user.img_url ,"token":user.device_token}
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": user_info }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = getuserprofileinformation