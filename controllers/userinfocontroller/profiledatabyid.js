const UserProfile = require("../../models/users")
const getuserprofileinformation = function (req, res) {
    const id = req.id;
    const user_type=req.user_type;
    try {
        if(user_type==1){
        const user_id=req.params.id
        getuserprofiledata(user_id, (err, data) => {
            if (err) {
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    else
    {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Not Supported for Developer" })
    }
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserprofiledata = async (id, callback) => {
    try {
        //skills.[skill.categorydata]
        const user = await UserProfile.query().select("user_id","email","firstname","lastname","gender","mobile_no","city","bio","img_url").where({"user_id":id}).withGraphFetched('[experience,categoryinfo.categorydata,skillsinfo.skill,project.usermapingproject.skill,education,price]').modifyGraph('experience', builder => {
            builder.select('title',"start_date","end_date","experience_in_years","company_name","city")
        }).modifyGraph('skills', builder => {
            builder.select('')
        }).modifyGraph('project', builder => {
            builder.select('project_name','about')
        }).modifyGraph('education', builder => {
            builder.select('university','degree','major','start_date','end_date')
        })
        if (user) {
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": user }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = getuserprofileinformation