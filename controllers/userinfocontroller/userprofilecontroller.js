const UserProfile = require("../../models/users")
const userprofileinformation = function (req, res,next) {
    const id=req.id
    const { body } = req;
    try {
        if(req.file)
        var image_url=req.file.location;
        body["img_url"]=image_url
        insertuserprofiledata(body, id, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }



};
const insertuserprofiledata = async (body, id, callback) => {
    try {
        const user = await UserProfile.query().findOne({ "user_id": id }).patch(body);
        if (user) {
            data = { "success": true, "code": 200, "message": "data inserted SuccessFully" }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = userprofileinformation