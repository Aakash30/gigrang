const UserProfile = require("../../models/users")
const getuserprofileinformation = function (req, res) {
    const id = req.id;
    try {
        getuserprofiledata(id, (err, data) => {
            if (err) {
                console.log(err)
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        console.log(e)
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserprofiledata = async (id, callback) => {
    try {
        const user = await UserProfile.query().select("user_id","email","firstname","lastname","gender","mobile_no","city","bio","img_url").where({"user_id":id}).withGraphFetched('[experience,categoryinfo.categorydata,project.usermapingproject.skill,skillsinfo.skill,education,price]').modifyGraph('experience', builder => {
            builder.select('title','employment_type','company_name','city','start_date','end_date','experience_in_years','currently_working')
        }).modifyGraph('skills', builder => {
            builder.select('')
        })
        .modifyGraph('project', builder => {
            builder.select('project_name','about')
        }).modifyGraph('usermapingproject', builder => {
            builder.select('')
        })
        .modifyGraph('education', builder => {
            builder.select('university','degree','major','start_date','end_date')
        }).modifyGraph('price', builder => {
            builder.select('charges')
        })
        if (user) {
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": user[0] }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = getuserprofileinformation