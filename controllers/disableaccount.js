const Users = require("../models/users")
const UserFavourite = require("../models/userfavourite")
const disableaccount = function (req, res) {
    const id = req.id
    dis_account(id, (data, err) => {
        if (data) {
            res.send(data)
        }
        else {
            res.statusCode = 400
            res.send({ "success": false, "code": 400, "message": 'Server Error' })
        }

    })
}

const dis_account = async (id, callback) => {
    const user = await Users.query().findOne({ "user_id": id }).patch({ "admin_status": false });
    if (user) {
        const data=await UserFavourite.query().delete().where('customer_id', id);
        console.log(data)
        callback({ "success": true, "code": 200, "message": "Account Disable Successfully" }, undefined)
    }
    else {
        callback(undefined, "error")
    }
}

module.exports = disableaccount