const UserLeads = require("../../models/userleads")
const leadsinformation =  function (req, res) {
    const id=req.id //userid
    try {
        userleadsdata(id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const userleadsdata = async (id,callback) => {
    try {
        const leads = await UserLeads.query().where({"user_id":id,"invitationaccepted":false}).withGraphFetched("[clientdata]").modifyGraph('clientdata', builder => {
            builder.select('email', "firstname", "lastname","img_url","city","mobile_no")
        })
        if (leads) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful","data":leads}
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = leadsinformation