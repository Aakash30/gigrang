const UserLeads = require("../../models/userleads")
const User=require("../../models/users")
let sendPushNotification = require('../../middleware/push-notification').sendPushNotification;
const leadsinformation =  function (req, res) {
    const id=req.id //userid
    try {
        const lead_id=req.params.id
        acceptlead(id,lead_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const acceptlead = async (id,lead_id,callback) => {
    try {
        const leads = await(await UserLeads.query().findOne({"user_id":id,"leads_id":lead_id})).$query().patchAndFetch({"invitationaccepted":true})
        console.log(leads)
        const client_id=leads["client_id"]
        if (leads) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful,Lead Accepted"}
            const user=await User.query().findOne({"user_id":client_id})
            sendPushNotification({ 'content': "Your invitation accepted by user", 'deviceToken': user.device_token }, {});
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = leadsinformation