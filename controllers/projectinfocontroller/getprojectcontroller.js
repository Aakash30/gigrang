const UserProject = require("../../models/userproject")
const UserProjectMapping=require("../../models/UserProjectMapping")
const getuserprojectinformation = function (req, res) {
    const id=req.id;
    try {
        getuserprojectdata(id, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserprojectdata = async (id, callback) => {
    try {
        const project = await UserProject.query().where({ "user_id": id}).withGraphFetched('[usermapingproject.skill]')

        if (project) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": project }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = getuserprojectinformation