const UserProject = require("../../models/userproject")
const get_projectinformation_by_id = function (req, res) {
    const id=req.id;
    try {
        const project_id=req.params.id;
        getuserproject(id, project_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserproject = async (id,project_id ,callback) => {
    try {
        const project = await UserProject.query().findOne({ "user_id": id,"id":project_id }).withGraphFetched('[usermapingproject.skill]')
        if (project) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful","data":project}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = get_projectinformation_by_id