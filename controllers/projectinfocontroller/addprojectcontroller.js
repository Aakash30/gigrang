const UserProject = require("../../models/userproject")
const UserProjectMapping=require("../../models/UserProjectMapping")
const userprojectinformation =function (req, res) {
    const { body } = req;
    const id=req.id;
    try {
        insertuserproject(body, id, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }



};
const insertuserproject = async (body, id, callback) => {
    body["user_id"]=id
    let dataArr=[]
    console.log(body)
    try {
        const project = await UserProject.query().insert({"user_id":id,"project_name":body["project_name"],"about":body["about"]});
        const p_id=project.id
        body["technology"].forEach(element => {
            dataArr.push({"project_id":p_id,"technology" : element})
        });
        const addmapping=await UserProjectMapping.query().insert(dataArr)
        if (project && addmapping) {
            data = { "success": true, "code": 200, "message": "Project data inserted successFully" }
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = userprojectinformation