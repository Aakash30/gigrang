const UserProject= require("../../models/userproject")
const UserProjectMapping=require("../../models/UserProjectMapping")
const editprojectinformation = function (req, res) {
    const { body } = req;
    const id=req.id;
    try {
        const project_id=req.params.id;
        editproject(id, project_id,body,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const editproject= async (id,project_id,body ,callback) => {
    try {
        let dataArr=[]
        console.log(id,project_id)

        const project = await(await UserProject.query().findOne({ "user_id": id,"id":project_id })).$query().patchAndFetch({"project_name":body["project_name"],"about":body["about"]})
        console.log(project)
        const projectid=project.id;
        const projectskill=await UserProjectMapping.query().where({"project_id":projectid}).delete()
        body["technology"].forEach(element => {
            dataArr.push({"project_id":projectid,"technology" : element})
        });
        const addmapping=await UserProjectMapping.query().insert(dataArr)
        if (addmapping) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = editprojectinformation