const UserProject= require("../../models/userproject")
const deleteProjectinformation =function (req, res) {
    const id=req.id;
    try {
        const project_id=req.params.id;
        deleteproject(id, project_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const deleteproject = async (id,project_id,callback) => {
    try {
        const project = await UserProject.query().delete().where({ "user_id": id,"id":project_id })
        
        if (project) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Delete Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = deleteProjectinformation