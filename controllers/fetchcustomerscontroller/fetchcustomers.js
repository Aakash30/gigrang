const User = require("../../models/users")
const UserExperience = require("../../models/userexperience")
const getuserinformation = function (req, res) {
    const id = req.id;
    const user_type = req.user_type
    const params = req.query.sortby
    console.log(params)
    console.log(user_type)
    try {
        if (user_type == 1) {
            getuserdata(id, req.body, params, (err, data) => {
                if (err) {
                    console.log(err)
                    res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "Server Error" })
                }
                else if (data) {
                    res.send(data)
                }
            })
        }
        else {
            res.statusCode = 400
            res.send({ "success": false, "code": 403, "message": "Not Supported for Developer" })
        }
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserdata = async (id, body, params, callback) => {
    const page = body["page"]
    let user = undefined
    let length = body["length"];
    let order_by = 'users.user_id'
    let order = 'ASC'
    if (params == 'min_sal') {
        order = "ASC"
        order_by = 'charges'
    }
    else if (params == 'max_sal') {
        order_by = 'charges'
        order = 'DESC'
    }
    else if (params == 'newest') {
        order_by = 'users.user_id'
        order = 'DESC'
    }
    console.log(order_by, order)
    try {
        if (body["category"] != undefined) {
            user = await User.query().select("users.user_id", "email", "firstname", "lastname", "img_url").where({ "user_type": 2, "verified": true }).innerJoinRelated('[categoryinfo,price]').withGraphFetched('[experience,categoryinfo.[categorydata],skillsinfo.skill,project.usermapingproject.skill,education,price]').
                modifyGraph('experience', builder => {
                    builder.select('title', "start_date", "end_date", "experience_in_years", "company_name")
                }).
                modifyGraph('categoryinfo', builder => {
                    builder.select('*').where((builder) => {
                        return builder.where({ "category_id": body["category"] })
                    })
                }).
                modifyGraph('price', builder => {
                    builder.select('charges')
                })
                .where(builder => {
                    builder.where({ "categoryinfo.category_id": body["category"] })
                    builder.where("admin_status", true)
                }).orderBy(order_by, order).page(page, length)
        }
        else {
            user = await User.query().select("users.user_id", "email", "firstname", "lastname", "img_url").where({ "user_type": 2, "verified": true }).page(page, length)
                .innerJoinRelated('price')
                .withGraphFetched('[experience,categoryinfo.categorydata,skillsinfo.skill,project.usermapingproject.skill,education,price]').modifyGraph('experience', builder => {
                    builder.select('title', "start_date", "end_date", "experience_in_years", "company_name")
                }).modifyGraph('skills', builder => {
                    builder.select('')
                }).modifyGraph('price', builder => {
                    builder.select('charges')
                }).where("admin_status", true).orderBy(order_by, order)
        }
        if (user) {
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": user }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = getuserinformation