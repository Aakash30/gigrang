const UserExperience = require("../../models/userexperience")
const get_experienceinformation_by_id = function (req, res) {
    const id=req.id;
    try {
        const experience_id=req.params.id;
        getuserexperiencedata(id, experience_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
function calculateexperience(start_date, end_date, flag) {
    let date1 = undefined
    let date2 = undefined

    if (flag) {
        var startdate_data = start_date.split("/")
        var enddate_data = end_date.split("/")
        date1 = new Date(startdate_data[0] + "/" + "01" + "/" + startdate_data[1]);
        date2 = new Date(enddate_data[0] + "/" + "01" + "/" + enddate_data[1]);

    }
    else {
        var startdate_data = start_date.split("/")
        date1 = new Date(startdate_data[0] + "/" + "01" + "/" + startdate_data[1]);
        date2 = new Date(end_date);
    }
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    console.log(diffTime)
    return parseFloat((diffDays / 365).toFixed(1))
}
function getCurrentDate() {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    let year = date_ob.getFullYear();
    console.log(year + "-" + month + "-" + date);
    //06/16/2018
    return (month + "/" + date + "/" + year)
}
const getuserexperiencedata = async (id,experience_id ,callback) => {
    try {
        const experience = await UserExperience.query().findOne({ "user_id": id,"id":experience_id })
        console.log(experience)
        if(experience["currently_working"])
        {
        experience["experience_in_years"]=calculateexperience(experience["start_date"],getCurrentDate(),false)
        }
        if (experience) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful","data":experience}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = get_experienceinformation_by_id