const UserExperience= require("../../models/userexperience")

const deleteExperienceinformation = function (req, res) {
    const id=req.id;
    try {
        const experience_id=req.params.id;
        deleteuserexperience(id, experience_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const deleteuserexperience = async (id,experience_id,callback) => {
    try {
        const experience = await UserExperience.query().delete().where({ "user_id": id,"id":experience_id })
        if (experience) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Delete Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = deleteExperienceinformation