const UserExperience = require("../../models/userexperience")
const userexperienceinformation = function (req, res) {
    const id = req.id
    const { body } = req;
    try {
        insertuserexperiencedata(body, id, (err, data) => {
            if (err) {
                res.statusCode = 400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
function calculateexperience(start_date, end_date, flag) {
    let date1 = undefined
    let date2 = undefined

    if (flag) {
        var startdate_data = start_date.split("/")
        var enddate_data = end_date.split("/")
        date1 = new Date(startdate_data[0] + "/" + "01" + "/" + startdate_data[1]);
        date2 = new Date(enddate_data[0] + "/" + "01" + "/" + enddate_data[1]);

    }
    else {
        var startdate_data = start_date.split("/")
        date1 = new Date(startdate_data[0] + "/" + "01" + "/" + startdate_data[1]);
        date2 = new Date(end_date);
    }
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    console.log(diffTime)
    return parseFloat((diffDays / 365).toFixed(1))
}
function getCurrentDate() {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    let year = date_ob.getFullYear();
    console.log(year + "-" + month + "-" + date);
    //06/16/2018
    return (month + "/" + date + "/" + year)
}

const insertuserexperiencedata = async (body, id, callback) => {
    body["user_id"] = id
    var flag = true
    if (body["currently_working"]) {
        var flag = false
        body["end_date"] = getCurrentDate()
    }
    const year = calculateexperience(body["start_date"], body["end_date"], flag)
    body["experience_in_years"] = year
    try {
        const experience = await UserExperience.query().insert(body);
        if (experience) {
            data = { "success": true, "code": 200, "message": "Experience data inserted successFully" }
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = userexperienceinformation