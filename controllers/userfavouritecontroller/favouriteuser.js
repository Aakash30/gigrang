const UserFavourite = require("../../models/userfavourite")
const userfavouriteinformation = function (req, res) {
    const id = req.id //userid
    const user_type = req.user_type
    const { body } = req;
    try {
        if (user_type == 1) {
            userfavouritedata(id, body, (err, data) => {
                if (err) {
                    console.log(err)
                    res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "Server Error" })
                }
                else if (data) {
                    res.send(data)
                }
            })
        }
        else {
            res.statusCode = 400
            res.send({ "success": false, "code": 403, "message": "Not Supported for Developer" })
        }
    }
    catch (e) {
        console.log(e)
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const userfavouritedata = async (id, body, callback) => {
    body["user_id"] = id
    try {
        const favourite_user = await UserFavourite.query().findOne({ "user_id": id, "customer_id": body["customer_id"] })
        console.log(favourite_user)
        if (favourite_user == undefined) {
            const favourite = await UserFavourite.query().insert(body);
            if (favourite) {
                //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
                data = { "success": true, "code": 200, "message": "Operation Successful" }
                callback(undefined, data)
            }
        }
        else {
            await UserFavourite.query().findOne({ "user_id": id, "customer_id": body["customer_id"] }).patch(body)
            data = { "success": true, "code": 200, "message": "Operation Successful" }
            callback(undefined, data)
        }
    }

    catch (e) {
        callback(e, undefined)
    }
}
module.exports = userfavouriteinformation