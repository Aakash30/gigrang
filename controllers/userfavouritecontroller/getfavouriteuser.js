const UserFavourite = require("../../models/userfavourite")
const userfavouriteinformation = function (req, res) {
    const id = req.id //userid
    const user_type = req.user_type
    try {
        if (user_type == 1) {
            userfavouritedata(id, (err, data) => {
                if (err) {
                    console.log(err)
                    res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "Server Error" })
                }
                else if (data) {
                    res.send(data)
                }
                else if (err == undefined) {
                    // res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "No Favourite Present", "data": [] })
                }
            })
        }
        else {
            res.statusCode = 400
            res.send({ "success": false, "code": 403, "message": "Not Supported for Developer" })
        }
    }
    catch (e) {
        console.log(e)
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const userfavouritedata = async (id, callback) => {
    try {
        const favourite_user = await UserFavourite.query().where({ "user_id": id, "favourite": true }).withGraphFetched("[userdata.[experience,price,skillsinfo.skill]]").modifyGraph('userdata', builder => {
            builder.select('email', "firstname", "lastname", "img_url", "city", "mobile_no")
            builder.where({"admin_status":true})
        });
        if (favourite_user) {
            const data = { "success": true, "code": 200, "message": "operation successfull", "data": favourite_user }
            callback(undefined, data)
        }
        else if (favourite_user.length == 0) {
            callback(undefined, undefined)
        }
    }

    catch (e) {
        callback(e, undefined)
    }
}
module.exports = userfavouriteinformation