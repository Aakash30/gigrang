const InviteLeads = require("../../models/userleads")
const User =require("../../models/users")
let sendPushNotification = require('../../middleware/push-notification').sendPushNotification;
const leadsinformation = function (req, res) {
    const id = req.id //userid
    const { body } = req;
    const user_type = req.user_type
    try {
        if (user_type == 1) {
            inviteuser(id, body, (err, data) => {
                if (err) {
                    res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "Server Error" })
                }
                else if (data) {
                    res.send(data)
                }
                else if(err==undefined)
                {
                    res.statusCode = 400
                    res.send({ "success": false, "code": 403, "message": "Invite sent Already" })
                }
            })
        }
        else
        {
            res.statusCode = 400
            res.send({ "success": false, "code": 403, "message": "Not Supported for Developer" })
        }
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const inviteuser = async (id, body, callback) => {
    body["client_id"] = id
    try {
        const checkleads=await InviteLeads.query().findOne({"client_id":id,"user_id":body["user_id"]})
        console.log(checkleads)
        if(checkleads!=undefined)
        {
        console.log(checkleads)
        callback(undefined,undefined)
      
        }
        else
        {
        const leads = await InviteLeads.query().insert(body);
        const user=await User.query().findOne({"user_id":body["user_id"]})
        const deviceToken=user.device_token
        
        if (leads) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Invite Sent Successfully" }
            let message = { "title": "Invite", "notificationType": 1, "message": "Received Invite", "notificationId": "101"};
            sendPushNotification({ 'content': message, 'deviceToken': deviceToken }, {});
            callback(undefined, data)
        }
    }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = leadsinformation