const UserEducation = require("../../models/userseducation")
const deleteEducationinformation =  function (req, res) {
    const id=req.id;
    try {
        const education_id=req.params.id;
        deleteusereducationdata(id, education_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const deleteusereducationdata = async (id,education_id ,callback) => {
    try {
        const education = await UserEducation.query().delete().where({ "user_id": id,"id":education_id })
        if (education) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Delete Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = deleteEducationinformation