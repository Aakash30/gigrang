const UserEducation = require("../../models/userseducation")
const get_educationinformation_by_id = function (req, res) {
    const id=req.id;
    try {
        const education_id=req.params.id;
        getusereducationdata(id, education_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getusereducationdata = async (id,education_id ,callback) => {
    try {
        const education = await UserEducation.query().findOne({ "user_id": id,"id":education_id })
        console.log(education)
        if (education) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful","data":education}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = get_educationinformation_by_id