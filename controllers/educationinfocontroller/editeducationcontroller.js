const UserEducation = require("../../models/userseducation")
const editeducationinformation =  function (req, res) {
    const id=req.id
    const { body } = req;
    try {
        const education_id=req.params.id;
        editusereducationdata(id, education_id,body,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
function calculateexperience(start_date,end_date)
{
const date1 = new Date(start_date);
const date2 = new Date(end_date);
const diffTime = Math.abs(date2 - date1);
const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
return parseFloat((diffDays/365).toFixed(1))
}
const editusereducationdata = async (id,education_id,body ,callback) => {
    const year=calculateexperience(body["start_date"],body["end_date"])
    body["education_duration"]=year
    try {
        const education = await UserEducation.query().findOne({ "user_id": id,"id":education_id }).patch(body);
        
        if (education) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = editeducationinformation