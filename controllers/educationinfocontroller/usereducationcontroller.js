const UserEducation = require("../../models/userseducation")
const usereducationinformation = function (req, res) {
    const { body } = req;
    const id=req.id;
    try {
        insertusereducationdata(body, id, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
function calculateexperience(start_date,end_date)
{
const date1 = new Date(start_date);
const date2 = new Date(end_date);
const diffTime = Math.abs(date2 - date1);
const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
return parseFloat((diffDays/365).toFixed(1))
}

const insertusereducationdata = async (body, id, callback) => {
    body["user_id"]=id
    const year=calculateexperience(body["start_date"],body["end_date"])
    body["education_duration"]=year
    console.log(body)
    try {
        const education = await UserEducation.query().insert(body);
        if (education) {
            data = { "success": true, "code": 200, "message": "Education data inserted successFully" }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = usereducationinformation