'use strict';
const jwt = require('jsonwebtoken');
//const secretkey = require("crypto").randomBytes(64).toString('hex')
const secretkey = "secretkey";
const bcrypt = require('bcryptjs')
const Users = require("../../models/users")
const logininformation = function (req, res) {
    user_is_available(req, res);
}
const updatetokentodb = async (access_token,deviceId,deviceType,deviceToken,email) => {
    const user = await Users.query().findOne({ "email": email }).patch({ "token": access_token,"deviceid":deviceId,"device_token":deviceToken,"device_type":deviceType})
}
const user_is_available = async (req, res) => {
    const { email,password,deviceType,deviceId,deviceToken} = req.body;
    try {
        const user = await Users.query().findOne({ "email": email });
        console.log(user)
        console.log(user)
        if (user == undefined) {
            res.statusCode = 400
            res.send({
                "code": 403,
                "message": "User does not exist"
            });
        }
        else if (!user.admin_status) {
            res.statusCode = 400
            res.send({
                "code": 403,
                "message": "Account is Disable please contact Admin"
            })
        }
        else if (!user.verified) {
            res.statusCode = 400
            res.send({
                "code": 403,
                "message": "Verify your account first"
            })
        }
        else {
            bcrypt.compare(password, user.password, (err, response) => {
                if (response) {
                    var access_token = jwt.sign({ "id": user.user_id, "email": user.email }, secretkey)
                    updatetokentodb(access_token,deviceId,deviceType,deviceToken,email)
                    res.send({
                        "code": 200,
                        "data":
                        {
                            "email": user.email,
                            "fullname": user.fullName(),
                            "firstname": user.firstname,
                            "lastname": user.lastname
                        },
                        "message": "Login SuccessFull",
                        "token": access_token
                    })
                }
                else if (response == false) {
                    res.statusCode = 400;
                    res.send({
                        "code": 400,
                        "message": "Password is incorrect"
                    })
                }


            })
        }
    } catch (e) {
        console.log(e)
    }
}

module.exports = logininformation


