//const {insertuser}=require("../../query/dbquery")
const bcrypt = require('bcryptjs')
const saltRounds = 10
const Users = require("../../models/users")
const email_verification=require("../../middleware/email")
const userinformation = function (req, res) {
    bcrypt.hash(req.body.password, saltRounds, (err, encrypted) => {
        if (err)
        console.log(err)
        req.body.password = encrypted
        insertuser(req.body, (err, data) => {
            if (err != undefined) {
                if (err.statusCode)
                    res.send({ "code": "400", "success": false, "message": "Server Error,Mising Information" })
                
                res.statusCode = 400;
                res.send({ "code": "400", "success": false, "message": "User Already Exist" })
            }
            else
            { 
                const code=generate(6)
                savecode(req.body.email,code)
                email_verification.sendEmail(req.body.email,code)
                res.send({ "success": true, "code": 200, "message": "Sign-Up SuccessFull,Verification code sent on your Email" })
            }
            })
    })
};
const savecode=async(email,code)=>
{
    await Users.query().findOne({ "email": email }).patch({ "email_verification_code": code })
}
function generate(n) {
    var add = 1, max = 12 - add; 

    if ( n > max ) {
            return generate(max) + generate(n - max);
    }
    max        = Math.pow(10, n+add);
    var min    = max/10;
    var number = Math.floor( Math.random() * (max - min + 1) ) + min;
    return ("" + number).substring(add); 
}
const insertuser = async (body, callback) => {
    try {
        const user = await Users.query().insert(body);
        data = { "success": true, "code": 200, "message": "Sign-Up SuccessFull" }
        callback(undefined, data)
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = userinformation