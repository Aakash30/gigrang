const email_verification = require("../middleware/sendcode")
const Users = require("../models/users")
function generate(n) {
    var add = 1, max = 12 - add;

    if (n > max) {
        return generate(max) + generate(n - max);
    }
    max = Math.pow(10, n + add);
    var min = max / 10;
    var number = Math.floor(Math.random() * (max - min + 1)) + min;
    return ("" + number).substring(add);
}
const forgotpassword = function (req, res) {
    const code = generate(4)
    savecode(req.body.email, code, (data, err) => {
        if (err) {
            res.statusCode = 400
            res.send({ "success": false, "code": 400, "message": "Email doesn't exist" })
        }
        else if(data==undefined)
        {
            res.statusCode = 400
            res.send({ "success": false, "code": 400, "message": "Email account is not Verified,Verify First" })
        }
        else {
            email_verification.sendEmail(req.body.email, code)
            res.send({ "success": true, "code": 200, "message": "Otp Sent to the email" })
        }
    })

}
const savecode = async (email, code, callback) => {
    const userdata = await Users.query().findOne({ "email": email })
    if (userdata==undefined) {
        callback(undefined, "error")
    }
    else if (!userdata.verified) {
        callback(undefined, undefined)
    }
    else if (userdata) {
        const data=await userdata.$query().patchAndFetch({ "otp_code": code })
        if(data)
        {
        callback(1, undefined)
        }
    }
}

module.exports = forgotpassword