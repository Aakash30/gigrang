const UserSkills= require("../../models/userskills")
const editskillsinformation =  function (req, res) {
    const id=req.id
    const { body } = req;
    try {
        const category_id=req.params.id
        editskills(id,category_id,body,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const editskills = async (id,category_id,body ,callback) => {
    let dataArr=[]
    try {
        const deletedata=await UserSkills.query().delete().where({"user_category_id":category_id,"user_id":id})
        console.log(deletedata)
        body["technology"].forEach(element => {
            dataArr.push({"technology" : element, "user_id": id,"user_category_id":category_id})
        });
        const skills = await UserSkills.query().insert(dataArr)
        
        if (skills) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = editskillsinformation