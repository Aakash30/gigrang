const User = require("../../models/users")
const Skill=require("../../models/skills")
const UserSkills = require("../../models/userskills")
const UserCategory=require("../../models/UserCategory")
const getuserskillsinformation = function (req, res) {
    const id = req.id;
    try {
        const skills_id = req.query.id
        console.log(skills_id)// undefined
        getuserskillsdata(id, skills_id, (err, data) => {
            if (err) {
                res.statusCode = 400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserskillsdata = async (id, skills_id, callback) => {
const category=await UserCategory.query().select("").where({"user_id":id}).withGraphFetched("[categorydata,skilldata.skill]")
    try {

        if (category) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": category }
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = getuserskillsinformation