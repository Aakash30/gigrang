const UserSkills = require("../../models/userskills")
const UserCategory = require("../../models/UserCategory")
const adduserskillsinformation = function (req, res) {
    const { body } = req;
    const id = req.id;
    try {
        insertuserskillsdata(body, id, (err, data) => {
            if (err) {
                res.statusCode = 400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
            else if(err==undefined)
            {
                res.statusCode = 400
                res.send({ "success": false, "code": 403, "message": "Category Skills Already Present" })
            }
        })
    }
    catch (e) {
        res.statusCode = 400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const insertuserskillsdata = async (body, id, callback) => {
    let dataArr = []
    let category = undefined
    console.log(id)
    try {
        category = await UserCategory.query().where({ "user_id": id, "category_id": body["category"] })
        console.log(category.length)
        if (category.length!=0) {
            
            callback(undefined,undefined)
        }
        else if(category.length==0) {
            category = await UserCategory.query().insert({ "user_id": id, "category_id": body["category"] })
            const category_id = category.id

            body["technology"].forEach(element => {
                dataArr.push({ "technology": element, "user_id": id, "user_category_id": category_id })
            });

            const skills = await UserSkills.query().insert(dataArr);
            if (skills) {
                data = { "success": true, "code": 200, "message": "Skills inserted successFully" }
                callback(undefined, data)
            }
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = adduserskillsinformation