const UserSkills = require("../../models/userskills")
const Skills=require("../../models/skills")
const UserCategory =require("../../models/UserCategory")
const get_skillsinformation_by_id = function (req, res) {
    const id=req.id;
    try {
        const category_id=req.params.id
        getuserskillsdata(id, category_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getuserskillsdata = async (id,category_id ,callback) => {
    try {
        const skills = await UserCategory.query().where({"user_id":id,"category_id":category_id}).withGraphFetched("skilldata.skill")
        if (skills) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful","data":skills}
            callback(undefined, data)
        }
    }
    catch (e) {
        console.log(e)
        callback(e, undefined)
    }
}
module.exports = get_skillsinformation_by_id