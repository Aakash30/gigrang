const UserSkills= require("../../models/userskills")
const UserCategory=require("../../models/UserCategory")

const deleteSkillsinformation = function (req, res) {
    const id=req.id;
    try {
        const category_id=req.params.id;
        deleteuserskills(id, category_id,(err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const deleteuserskills = async (id,category_id,callback) => {
    try {
        const skill = await UserCategory.query().delete().where({"category_id":category_id,"user_id":id})
        if (skill) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Delete Operation Successful"}
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = deleteSkillsinformation