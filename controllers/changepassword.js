const Users = require("../models/users")
const bcrypt = require('bcryptjs')
const saltRounds = 10
const resetpassword = function (req, res) {
    const id=req.id
    const password = req.body.password
    passwordreset(id, password, (data,err) => {
        if (data) {
            res.send(data)
        }
        else {
            res.statusCode = 400
            res.send({ "success": false, "code": 400, "message": 'Server Error' })
        }

    })
}
const updateuserpassword=async(encrypted,id)=>
{
    await Users.query().findOne({"user_id":id}).patch({"password":encrypted})
}
const passwordreset = async (id, password, callback) => {
    const user = await Users.query().findOne({ "user_id": id });
    if (user) {
        bcrypt.compare(password, user.password, (err, response) => {
            if (response) {
                callback({ "success": false, "code": 200, "message": 'Your password must be different from your existing password' }, undefined)
            }
            else if (response == false) {
                bcrypt.hash(password, saltRounds, (err, encrypted) => {
                    console.log(encrypted)
                    if (err)
                    callback(undefined, "error")

                    updateuserpassword(encrypted,id)
                    
                    callback({ "success": true, "code": 200, "message": 'Your password is Successfully changed' }, undefined)

                })
            }
        })
    }
    else {
        callback(undefined, "error")
    }

}

module.exports = resetpassword