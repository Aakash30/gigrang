'use strict';
const aws = require('aws-sdk');
const {creds,bucketName} = require('../config/aws');
const multer = require('multer');
const multerS3 = require('multer-s3');
//AWS.config.creds;
aws.config.update(creds)
const s3 = new aws.S3();
const acl = 'public-read';
const metaData = function (req, file, cb) {
    cb(null, {
        fieldName: file.fieldname
    });
}

const user_image = multer({
    storage: multerS3({
        s3: s3,
        acl: acl,
        bucket: bucketName,
        metadata: metaData,
        key: function (req, file, cb) {
            cb(null, 'userimage/' + Date.now().toString() + '_' + file.originalname)
        }
    })
}); 
module.exports = {
    user_image
};