const UserPrice = require("../../models/userprice")

const userpriceinformation = function (req, res) {
    const id=req.id;
    const { body } = req;
    try {
        insertprice(body, id, (err, data) => {
            if (err) {
                console.log(err)
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        console.log(e)
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }



};
const insertprice = async (body, id, callback) => {
    body["user_id"]=id
    try {
        const project = await UserPrice.query().findOne({ "user_id": id }).patch(body);
        if(project==0)
        {
            await UserPrice.query().insert(body);
            data = { "success": true, "code": 200, "message": "Price data inserted successFully" }
            callback(undefined, data)
        }
        if (project) {
            data = { "success": true, "code": 200, "message": "Price data inserted successFully" }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = userpriceinformation