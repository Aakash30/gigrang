const Category = require("../../models/Category")
const categoryinformation =  function (req, res) {
    const id=req.id
    const { body } = req;
    try {
        categorydata(id,body, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const categorydata = async (id, body,callback) => {
    try {
        const category = await Category.query().insert(body)
        if (category) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful"}
            callback(undefined, data)
        }
  
    }
    catch (e) {
        callback(e, undefined)
    }
}

module.exports = categoryinformation