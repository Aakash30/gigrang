const Skills = require("../../models/skills")
const skillsinformation =  function (req, res) {
    const id=req.id
    const { body } = req;
    try {
        skillsdata(id,body, (err, data) => {
            if (err) {
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const skillsdata = async (id, body,callback) => {
    try {
        const skills = await Skills.query().insert(body)
        if (skills) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful"}
            callback(undefined, data)
        }
  
    }
    catch (e) {
        callback(e, undefined)
    }
}

module.exports = skillsinformation