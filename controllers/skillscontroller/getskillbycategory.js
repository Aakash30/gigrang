const Skills = require("../../models/skills")
const skills = function (req, res) {
    const id=req.id
    try {
        const category_id=req.params.id
        getskills(id,category_id,(err, data) => {
            if (err) {
                console.log(err)
                res.statusCode=400
                res.send({ "success": false, "code": 403, "message": "Server Error" })
            }
            else if (data) {
                res.send(data)
            }
        })
    }
    catch (e) {
        console.log(err)
        res.statusCode=400
        res.send({ "success": false, "code": 403, "message": "Server Error" })
    }
};
const getskills = async (id, category_id, callback) => {
    try {
        const skills = await Skills.query().where({"category_id":category_id})
        if (skills) {
            //const user_info = { "email": user.email, "firstname": user.firstname, "lastname": user.lastname, "about": user.bio, "user_type": user.user_type, "gender": user.gender, "city": user.city, "mobile_no": user.mobile_no }
            data = { "success": true, "code": 200, "message": "Operation Successful", "data": skills }
            callback(undefined, data)
        }
    }
    catch (e) {
        callback(e, undefined)
    }
}
module.exports = skills