const Users = require("../models/users")
const bcrypt = require('bcryptjs')
const saltRounds = 10
const resetpassword = function (req, res) {
    const email = req.body.email
    const password = req.body.password
    passwordreset(email, password, (data, err) => {
        if (err) {
            res.statusCode = 400
            res.send({ "success": false, "code": 400, "message": 'Server Error' })

        }
        else if (data == undefined) {
            res.statusCode = 400;
            res.send({ "success": false, "code": 200, "message": 'Your password must be different from your existing password' });
        }
        else if (data) {
            res.send(data)
        }


    })
}
const updateuserpassword = async (encrypted, email) => {
    await Users.query().findOne({ "email": email }).patch({ "password": encrypted })
}
const passwordreset = async (email, password, callback) => {
    const user = await Users.query().findOne({ "email": email });
    if (user) {
        bcrypt.compare(password, user.password, (err, response) => {
            if (response) {
                callback(undefined, undefined)
            }
            else if (response == false) {
                bcrypt.hash(password, saltRounds, (err, encrypted) => {
                    console.log(encrypted)
                    if (err)
                        callback(undefined, "error")

                    updateuserpassword(encrypted, email)

                    callback({ "success": true, "code": 200, "message": 'Your password is Successfully changed' }, undefined)

                })
            }
        })
    }
    else {
        callback(undefined, "error")
    }

}

module.exports = resetpassword