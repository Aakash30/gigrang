const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
//const logininformation=require("../../controllers/userController/loginController")
const inviteuser=require("../../controllers/inviteusercontrollers/inviteuser")

router.route("/invite").all((req,res,next)=>
{
next()
}).get((req,res)=>
{
res.send("Get operation yet not supported")
}).put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),inviteuser)


module.exports=router;