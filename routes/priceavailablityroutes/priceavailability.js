const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const addpriceavailability=require("../../controllers/priceAvailabilityController/addPriceAvailabilityController")
const getprice=require("../../controllers/priceAvailabilityController/getpricecontroller")
router.route("/price").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getprice).put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),addpriceavailability)


module.exports=router;