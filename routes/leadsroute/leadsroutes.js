const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
//const logininformation=require("../../controllers/userController/loginController")
const getleadinformation=require("../../controllers/leadsinfocontroller/getuserleadscontroller")
const  getacceptedlead=require("../../controllers/leadsinfocontroller/getacceptedleads")
const acceptlead=require("../../controllers/leadsinfocontroller/acceptlead")

router.route("/leadsinvite").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
  session: false
}),getleadinformation)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post((req,res)=>
{
res.send("Post operation yet not supported")
})

router.route("/leadsinvite/:id").all((req,res,next)=>
{
next()
}).get((req,res)=>
{
res.send("Get operation yet not supported")
})
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
  session: false
}),acceptlead)

router.route("/myleads").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
  session: false
}),getacceptedlead)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post((req,res)=>
{
res.send("Post operation yet not supported")
})



module.exports=router;