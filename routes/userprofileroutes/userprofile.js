const express=require("express")
const passport = require('passport');
require('../../middleware/passport')(passport);
const router=express.Router()
const awsfile=require("../../controllers/aws")
var multer  = require('multer')

const userprofileinformation=require("../../controllers/userinfocontroller/userprofilecontroller")
const getuserprofileinformation=require("../../controllers/userinfocontroller/getuserprofilecontroller")
const getprofile=require("../../controllers/userinfocontroller/getuserdatacontroller")

router.route("/profile").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getuserprofileinformation)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),awsfile.user_image.single("profile_picture"),userprofileinformation)


router.route("/profile/myprofile").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getprofile)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post((req,res)=>
{
res.send("Post operation yet not supported")
})

module.exports=router;