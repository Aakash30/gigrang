const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const adduserexperience=require("../../controllers/experienceinfocontroller/addexperiencecontroller")
const getuserexperience=require("../../controllers/experienceinfocontroller/getuserexperiencecontroller")
const edituserexperience=require("../../controllers/experienceinfocontroller/editexperiencecontroller")
const deleteuserexperience=require("../../controllers/experienceinfocontroller/deleteexperiencecontroller")
const experiencedatabyid=require("../../controllers/experienceinfocontroller/experiencedatabyid")
router.route("/experience").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getuserexperience)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),adduserexperience)

router.route("/experience/:id").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),experiencedatabyid)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete(passport.authenticate('jwt', {
    session: false
  }),deleteuserexperience).post(passport.authenticate('jwt', {
    session: false
  }),edituserexperience)

module.exports=router;