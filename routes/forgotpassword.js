const express=require("express")
const router=express.Router()
const forgotpassword=require("../controllers/forgotpassword")
router.route("/forgotpassword").all((req,res,next)=>
  {
  next()
  }).get((req,res)=>
  {
  res.send("Get operation yet not supported")
  })
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(forgotpassword)

  module.exports=router
