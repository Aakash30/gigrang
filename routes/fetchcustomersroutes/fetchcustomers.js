const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
//const logininformation=require("../../controllers/userController/loginController")
const fetchcustomers=require("../../controllers/fetchcustomerscontroller/fetchcustomers")
router.route("/fetchcustomers").all((req,res,next)=>
{
next()
}).get((req,res)=>
{
res.send("Get operation yet not supported")
}).put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
  session: false
}),fetchcustomers)




module.exports=router;