const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
//const logininformation=require("../../controllers/userController/loginController")
const categorycontrollers=require("../../controllers/categorycontrollers/addcategorycontrollers")
const getcategorycontrollers=require("../../controllers/categorycontrollers/getcategorycontroller")
router.route("/category").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getcategorycontrollers).put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),categorycontrollers)

module.exports=router