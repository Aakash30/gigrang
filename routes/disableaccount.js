const express=require("express")
const router=express.Router()
const disable_account=require("../controllers/disableaccount")
const passport = require('passport');
require('../middleware/passport')(passport);
router.route("/disableaccount").all((req,res,next)=>
  {
  next()
  }).get((req,res)=>
  {
  res.send("Get operation yet not supported")
  })
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(passport.authenticate('jwt', {
    session: false
  }),disable_account)

  module.exports=router
