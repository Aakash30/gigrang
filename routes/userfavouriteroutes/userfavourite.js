const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const favourite=require("../../controllers/userfavouritecontroller/favouriteuser")
const getfavourite=require("../../controllers/userfavouritecontroller/getfavouriteuser")
router.route("/favourite").all((req,res,next)=>
  {
  next()
  }).get(passport.authenticate('jwt', {
    session: false
  }),getfavourite)
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(passport.authenticate('jwt', {
    session: false
  }),favourite)

  module.exports=router
