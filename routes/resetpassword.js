const express=require("express")
const router=express.Router()
const resetpassword=require("../controllers/resetpassword")
router.route("/resetpassword").all((req,res,next)=>
  {
  next()
  }).get((req,res)=>
  {
  res.send("Get operation yet not supported")
  })
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(resetpassword)

  module.exports=router
