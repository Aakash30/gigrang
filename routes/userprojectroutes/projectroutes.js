const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const addprojectcontroller=require("../../controllers/projectinfocontroller/addprojectcontroller")
const getprojectcontroller=require("../../controllers/projectinfocontroller/getprojectcontroller")
const editprojectcontroller=require("../../controllers/projectinfocontroller/editprojectcontroller")
const deleteprojectcontroller=require("../../controllers/projectinfocontroller/deleteprojectcontroller")
const getprojectbyidcontroller=require("../../controllers/projectinfocontroller/projectdatabyid")
router.route("/project").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getprojectcontroller)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),addprojectcontroller)



router.route("/project/:id").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
  session: false
}),getprojectbyidcontroller)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete(passport.authenticate('jwt', {
    session: false
  }),deleteprojectcontroller).post(passport.authenticate('jwt', {
    session: false
  }),editprojectcontroller)




module.exports=router;