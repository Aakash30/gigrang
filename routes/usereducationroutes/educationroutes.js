const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const usereducationinformation=require("../../controllers/educationinfocontroller/usereducationcontroller")
const getusereducationinformation=require("../../controllers/educationinfocontroller/getusereducationcontroller")
const editusereducationinformation=require("../../controllers/educationinfocontroller/editeducationcontroller")
const delete_education_information=require("../../controllers/educationinfocontroller/deleteEducationinformation")
const educationdatabyid=require("../../controllers/educationinfocontroller/educationbyIdcontroller")
router.route("/education").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getusereducationinformation)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),usereducationinformation)

router.route("/education/:id").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),educationdatabyid)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete(passport.authenticate('jwt', {
    session: false
  }),delete_education_information).post(passport.authenticate('jwt', {
    session: false
  }),editusereducationinformation)

module.exports=router;