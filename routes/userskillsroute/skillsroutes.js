const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
const addskillscontroller=require("../../controllers/skillsinfocontroller/addskillscontroller")
const getskillscontroller=require("../../controllers/skillsinfocontroller/getskillscontroller")
const editskillscontroller=require("../../controllers/skillsinfocontroller/editskillscontroller")
const deleteskillscontroller=require("../../controllers/skillsinfocontroller/deleteskillscontroller")
const getskillsbyidcontroller=require("../../controllers/skillsinfocontroller/getskillbyidcontroller")
router.route("/profileskills").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getskillscontroller)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),addskillscontroller)

router.route("/profileskills/:id").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getskillsbyidcontroller)
.put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete(passport.authenticate('jwt', {
  session: false
}),deleteskillscontroller)
.post(passport.authenticate('jwt', {
    session: false
  }),editskillscontroller)
  
module.exports=router;