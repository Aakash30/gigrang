const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../middleware/passport')(passport);
const getprofilebyid=require("../controllers/userinfocontroller/profiledatabyid")
router.route("/:id").all((req,res,next)=>
  {
  next()
  }).get(passport.authenticate('jwt', {
      session: false
    }),getprofilebyid)
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post((req,res)=>
  {
  res.send("Post operation yet not supported")
  })

  module.exports=router
