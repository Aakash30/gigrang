const express=require("express")
const router=express.Router()
const passport = require('passport');
require('../../middleware/passport')(passport);
//const logininformation=require("../../controllers/userController/loginController")
const addskillsinformation=require("../../controllers/skillscontroller/addskillscontroller")
const getskillsinformation=require("../../controllers/skillscontroller/getskillscontroller")
const getskillsbycategory=require("../../controllers/skillscontroller/getskillbycategory")
router.route("/skills").all((req,res,next)=>
{
next()
}).get(passport.authenticate('jwt', {
    session: false
  }),getskillsinformation).put((req,res)=>
{
res.send("Put operation yet not supported")
}).delete((req,res)=>
{
res.send("Delete operation yet not supported")
}).post(passport.authenticate('jwt', {
    session: false
  }),addskillsinformation)

  router.route("/skills/:id").all((req,res,next)=>
  {
  next()
  }).get((req,res)=>
  {
  res.send("Get operation yet not supported")
  }).put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(passport.authenticate('jwt', {
      session: false
    }),getskillsbycategory)

  module.exports=router;