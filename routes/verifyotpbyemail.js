const express=require("express")
const router=express.Router()
const verifyotp=require("../controllers/verifyotp")
router.route("/verifyotp").all((req,res,next)=>
  {
  next()
  }).get((req,res)=>
  {
  res.send("Get operation yet not supported")
  })
  .put((req,res)=>
  {
  res.send("Put operation yet not supported")
  }).delete((req,res)=>
  {
  res.send("Delete operation yet not supported")
  }).post(verifyotp)

  module.exports=router
