const { Model } = require('objection');
class UserExperience extends Model {
  static get tableName() {
    return 'users_experienceinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        title:{ type: 'string', minLength: 1, maxLength: 255 },
        employment_type:{ type: 'string', minLength: 1, maxLength: 255 },
        company_name:{ type: 'string', minLength: 1, maxLength: 255 },
        city:{ type: 'string', minLength: 1, maxLength: 255 },
        start_date:{ type: 'string', minLength: 1, maxLength: 255 },
        end_date:{ type: 'string', minLength: 1, maxLength: 255 },
        experience_in_years:{ type: 'float'},
        currently_working:{ type: 'boolean'}
      }
    }
  };
}
module.exports=UserExperience