const { Model } = require('objection');
class UserFavourite extends Model {
  static get tableName() {
    return 'users_favouriteinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        customer_id:{ type: 'int'},
        favourite:{type:'boolean'}    
      }
    }
  };

  static get relationMappings() {
    const user=require("./../models/users")
    return {
      userdata: {
        relation: Model.BelongsToOneRelation,
        modelClass: user,
        join: {
          from: 'users_favouriteinfo.customer_id',
          to: 'users.user_id'
        }
      },

    
    }
  }
}
module.exports=UserFavourite