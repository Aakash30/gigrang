const { Model } = require('objection');
class UserProjectMapping extends Model
{
  static get tableName() {
    return 'users_projectmappinginfo';
  } 
  static get idColumn() {
    return 'map_id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
      map_id: { type: 'int'},
      project_id:{type:'int'},
      technology: { type: 'int'}  
      }
    }
  };
  static get relationMappings() {
    const skills = require("./../models/skills")
    const UserProject=require("./../models/userproject")
    return {
      skill: {
        relation: Model.BelongsToOneRelation,
        modelClass: skills,
        join: {
          from: 'users_projectmappinginfo.technology',
          to: 'skillsinfo.id'
        }
    }
  }
  }

}
module.exports=UserProjectMapping