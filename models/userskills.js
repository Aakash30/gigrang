const { Model } = require('objection');
class UserSkills extends Model {
  static get tableName() {
    return 'users_skillsinfo';
  }
  static get idColumn() {
    return 'id';
  }

  static get relationMappings() {
    const skills = require("./../models/skills")
    const users=require("./../models/users")
    const category=require("./../models/Category")
    const userCategory=require("./../models/UserCategory")
    return {
      categoryrelation: {
        relation: Model.BelongsToOneRelation,
        modelClass: userCategory,
        join: {
          from: 'users_skillinfo.user_category_id',
          to: 'users_categoryinfo.id'
        }
      },

      skill: {
        relation: Model.BelongsToOneRelation,
        modelClass: skills,
        join: {
          from: 'users_skillsinfo.technology',
          to: 'skillsinfo.id'
        }
      },
      // skilldata: {
      //   relation: Model.BelongsToOneRelation,
      //   modelClass: category,
      //   join: {
      //     from: 'users_skillsinfo.category_id',
      //     to: 'users_categoryinfo.id'
      //   }
      // },
    }
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int' },
        user_id: { type: 'int' },
        user_category_id: { type: "int" },
        technology: { type: "int[]" }
      }
    }
  };
}
module.exports = UserSkills