const { Model } = require('objection');
class Users extends Model {
  static get tableName() {
    return 'users';
  }
  static get idColumn() {
    return 'user_id';
  }
  fullName() {
    return this.firstname + ' ' + this.lastname;
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ["email", "password","firstname","lastname","user_type"],
      properties: {
        user_id: { type: 'int' },
        firstname: { type: 'string', minLength: 1, maxLength: 255 },
        lastname: { type: 'string', minLength: 1, maxLength: 255 },
        email: { type: 'string', minLength: 1, maxLength: 255 },
        device_type: { type: 'string', minLength: 1, maxLength: 255},
        user_type:{ type: 'int' },
        img_url: { type: 'string', minLength: 1, maxLength: 255 },
        password: { type: 'string', minLength: 1, maxLength: 255 },
        gender:{type:'string',minLength: 1, maxLength: 255},
        mobile_no: { type: 'int'},
        verified:{type:"boolean"},
        email_verification_code:{type:'int'},
        city: { type: 'string', minLength: 1, maxLength: 100 },
        token: { type: 'string', minLength: 1, maxLength: 255 },
        bio: { type: 'string', minLength: 1, maxLength: 255 },
        deviceid:{ type: 'string', minLength: 1, maxLength: 255 },
        device_token:{ type: 'string', minLength: 1, maxLength: 255 },
        admin_status:{type:"boolean"}
      }
    }
  };
  static get relationMappings () {
    const userseducation=require("./userseducation");
    const userexperience=require("./userexperience");
    const userfavourite=require("./userfavourite")
    const userproject=require("./userproject")
    const userskills=require("./userskills")
    const userCategory=require("./UserCategory")
    const userprice=require("./userprice")
    const userleads=require("./userleads")
    return {
      education: {
        relation: Model.HasManyRelation,
        modelClass: userseducation,
        join: {
          from: 'users.user_id',
          to: 'users_educationinfo.user_id'
        }
      },
      experience:{
        relation: Model.HasManyRelation,
        modelClass: userexperience,
        join: {
          from: 'users.user_id',
          to: 'users_experienceinfo.user_id'
        }
       },
       project:{
        relation: Model.HasManyRelation,
        modelClass: userproject,
        join: {
          from: 'users.user_id',
          to: 'users_projectinfo.user_id'
        }
       },
       price:{
        relation: Model.HasOneRelation,
        modelClass: userprice,
        join: {
          from: 'users.user_id',
          to: 'users_priceinfo.user_id'
        }
       },
       
       categoryinfo:{
        relation: Model.HasManyRelation,
        modelClass: userCategory,
        join: {
          from: 'users.user_id',
          to: 'users_categoryinfo.user_id'
        }
       },
       skillsinfo:{
        relation: Model.HasManyRelation,
        modelClass: userskills,
        join: {
          from: 'users.user_id',
          to: 'users_skillsinfo.user_id'
        }
       },
       leads:{
        relation: Model.HasManyRelation,
        modelClass: userleads,
        join: {
          from: 'users.user_id',
          to: 'users_leadinfo.user_id'
        }
       },
       favourite:{
        relation: Model.HasManyRelation,
        modelClass: userfavourite,
        join: {
          from: 'users.user_id',
          to: 'users_favouriteinfo.user_id'
        }
       }      
    }
  }
}
module.exports = Users