const { Model } = require('objection');
class UserLeads extends Model {
  static get tableName() {
    return 'users_leadsinfo';
  }
  static get idColumn() {
    return 'leads_id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        leads_id: { type: 'int'},
        client_id:{ type: 'int'},
        user_id: { type: 'int'},
        invitationAccepted:{type:'boolean'}    
      }
    }
  };

  static get relationMappings() {
    const user=require("./../models/users")
    return {
      clientdata: {
        relation: Model.BelongsToOneRelation,
        modelClass: user,
        join: {
          from: 'users_leadsinfo.client_id',
          to: 'users.user_id'
        }
      },

    
    }
  }

}
module.exports=UserLeads