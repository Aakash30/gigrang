const { Model } = require('objection');
class Category extends Model {
  static get tableName() {
    return 'categoryinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        title: { type: 'int'},
        image:{ type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  };
}
module.exports=Category