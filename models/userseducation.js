const { Model } = require('objection');
class UserEducation extends Model {
  static get tableName() {
    return 'users_educationinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        university:{type:'string', minLength: 1, maxLength: 255 },
        degree: { type: 'string', minLength: 1, maxLength: 255 },
        major: { type: 'string', minLength: 1, maxLength: 255 },
        start_date: { type: 'string', minLength: 1, maxLength: 100 },
        end_date: { type: 'string', minLength: 1, maxLength: 255 },
        education_duration:{type:'float'}
      }
    }
  };
}
module.exports=UserEducation