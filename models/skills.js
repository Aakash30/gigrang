const { Model } = require('objection');
class Skills extends Model {
  static get tableName() {
    return 'skillsinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        title:{ type: 'string', minLength: 1, maxLength: 255 },
        category_id:{ type: 'int'}
      }
    }
  };
  
  static get relationMappings() {
    const UserSkills = require('./userskills');
    const Category=require("./Category")
    
    return {
        userSkill: {
            relation: Model.HasManyRelation,
            modelClass: UserSkills,
            join: {
                to: 'users_skillsinfo.technology',
                from: 'skillsinfo.id'
            }
        },
        categorydata: {
          relation: Model.BelongsToOneRelation,
          modelClass: Category,
          //filter: query => query.select('categorydata.id'),
          join: {
              to: 'categoryinfo.id',
              from: 'skillsinfo.category_id'
          }
      }
    }
}


}
module.exports=Skills