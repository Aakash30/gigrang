const { Model } = require('objection');
class UserProfile extends Model {
  static get tableName() {
    return 'users_information';
  }
  static get idColumn() {
    return 'userprofile_id';
  }
  fullName() {
    return this.firstname + ' ' + this.lastname;
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        userprofile_id: { type: 'int'},
        gender:{type:'string'},
        mobile_no: { type: 'string', minLength: 1, maxLength: 20 },
        city: { type: 'string', minLength: 1, maxLength: 100 },
        bio: { type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  };
}
module.exports=UserProfile