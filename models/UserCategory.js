const { Model } = require('objection');
class UserCategory extends Model {
  static get tableName() {
    return 'users_categoryinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        category_id: { type: 'int'}
      }
    }
  };


  static get relationMappings() {
    const category=require("./../models/Category")
    const userskill=require("./../models/userskills")
    return {
      categorydata: {
        relation: Model.BelongsToOneRelation,
        modelClass: category,
        join: {
          from: 'users_categoryinfo.category_id',
          to: 'categoryinfo.id'
        }
      },
      skilldata: {
        relation: Model.HasManyRelation,
        modelClass: userskill,
        join: {
          from: 'users_categoryinfo.id',
          to: 'users_skillsinfo.user_category_id'
        }
      },

    
    }
  }
}
module.exports=UserCategory