const { Model } = require('objection');
class UserPrice extends Model {
  static get tableName() {
    return 'users_priceinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      required: ["charges","availability"],
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        charges:{ type: 'int'},
        availability:{ type: 'string', minLength: 1, maxLength: 255 }
      }
    }
  };
}
module.exports=UserPrice