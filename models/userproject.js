const { Model } = require('objection');
class UserProject extends Model {
  static get tableName() {
    return 'users_projectinfo';
  }
  static get idColumn() {
    return 'id';
  }
  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'int'},
        user_id: { type: 'int'},
        project_name:{ type: 'string', minLength: 1, maxLength: 255 },
        about:{ type: 'string', minLength: 1, maxLength: 255 }
        
      }
    }
  };
  static get relationMappings () {
    const userprojectmapping=require("../models/UserProjectMapping")
    return {
      usermapingproject:{
        relation: Model.HasManyRelation,
        modelClass: userprojectmapping,
        join: {
          from: 'users_projectinfo.id',
          to: 'users_projectmappinginfo.project_id'
        }
       },
             
    }
  }
}
module.exports=UserProject