const user = `
CREATE TABLE users (
    user_id  SERIAL PRIMARY KEY,
    email varchar UNIQUE,
    firstname text,
    lastname text,
    password varchar,
    user_type int,
    socialid varchar,
    email_verification_code int,
    verified boolean DEFAULT FALSE,
    otp_code int,
    deviceid varchar,
    device_type varchar,
    device_token varchar,
    token varchar,
    gender text,
    img_url varchar,
    mobile_no varchar,
    city text,
    bio text,
    admin_status boolean default true
);
`;
const user_socialinfo=`CREATE TABLE users_socialinfo (
    user_id  SERIAL PRIMARY KEY,
    email varchar UNIQUE,
    firstname text,
    lastname text,
    password varchar,
    user_type int,
    socialid varchar,
    email_verification_code int,
    verified boolean DEFAULT FALSE,
    deviceid varchar,
    device_type varchar,
    device_token varchar,
    token varchar,
    gender text,
    img_url varchar,
    mobile_no varchar,
    city text,
    bio text,
    admin_status boolean default true
);
`;

const user_educationinformation =`
 CREATE TABLE users_educationinfo(
     id SERIAL PRIMARY KEY,
     user_id int REFERENCES users(user_id),
     university text,
     start_date varchar(255),
     end_date varchar(255),
     degree varchar(255),
     major varchar(255),
     education_duration float    
 );`
;

const users_experienceinfo=`
CREATE TABLE users_experienceinfo(
    id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    title text,
    company_name text,
    city text,
    employment_type text,
    start_date varchar(255),
    end_date varchar(255),
    experience_in_years float,
    currently_working boolean
);`
;

const users_projectinfo=`
CREATE TABLE users_projectinfo(
    id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    project_name text,
    about text
);`
;

const users_categoryinfo=`
CREATE TABLE users_categoryinfo(
    id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    category_id int REFERENCES categoryinfo(id)
);`
;

const users_priceinfo=`
CREATE TABLE users_priceinfo(
    id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    charges int,
    availability varchar
);`
;

const users_skillsinfo=`
CREATE TABLE users_skillsinfo(
    id SERIAL PRIMARY KEY,
    user_category_id int REFERENCES users_categoryinfo(id) ON DELETE CASCADE,
    user_id int REFERENCES users(user_id),
    technology int REFERENCES skillsinfo(id)
);`
;

const skillsinfo=`
CREATE TABLE skillsinfo(
    id SERIAL PRIMARY KEY,
    category_id int REFERENCES categoryinfo(id),
    title varchar
);`
;

const categoryinfo=`
CREATE TABLE categoryinfo(
    id SERIAL PRIMARY KEY,
    title varchar,
    image varchar
);`
;


const users_leadsinfo=`
CREATE TABLE users_leadsinfo(
    leads_id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    client_id int REFERENCES users(user_id),
    invitationaccepted boolean default false
);`
;
const users_projectmappinginfo=`
CREATE TABLE users_projectmappinginfo(
    map_id SERIAL PRIMARY KEY,
    project_id int REFERENCES users_projectinfo(id) ON DELETE CASCADE,
    technology int REFERENCES skillsinfo(id)
);`;

const users_favouriteinfo=`
CREATE TABLE users_favouriteinfo(
    id SERIAL PRIMARY KEY,
    user_id int REFERENCES users(user_id),
    customer_id int REFERENCES users(user_id),
    favourite boolean
);`
;

// const user_information =`
// CREATE TABLE users_information(
//     userprofile_id int REFERENCES users(user_id) UNIQUE,
//     gender char,
//     mobile_no varchar,
//     city char,
//     bio varchar
// );`
;

// const insertuser=(data)=>
// {
// 'INSERT INTO users'
// VALUES (data.email, data.firstName, data.lastName,data.password);
// }

module.exports={
    user,
    users_categoryinfo,
    user_educationinformation,
    users_projectinfo,
    users_experienceinfo,
    users_priceinfo,
    users_skillsinfo,
    users_leadsinfo,
    skillsinfo,
    categoryinfo,
    users_projectmappinginfo,
    users_favouriteinfo
}