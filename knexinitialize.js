const { Model } = require('objection');
const Knex = require('knex');
// Initialize knex.
const knex = Knex({
  client: 'pg',
  useNullAsDefault: true,
  connection: {
    user: 'postgres',
    host: 'localhost',
    database: 'mydb',
    password: 'aakash30',
  }
});

// Give the knex instance to objection.
Model.knex(knex);